import tensorflow as tf
from tensorflow import keras
import logging

class ResidualUnit(keras.layers.Layer):
    def __init__(self, filters, dialation_rate, **kwargs):
        super(ResidualUnit, self).__init__(**kwargs)
        self.filters = filters
        self.dialation_rate = dialation_rate
        self.dial_conv = keras.layers.Conv1D(filters=self.filters,
                                            kernel_size=2,
                                            padding="causal",
                                            dilation_rate=self.dialation_rate)

    def build(self, input_shape):
         self.skip_conv = keras.layers.Conv1D(filters=input_shape[-1],
                                        kernel_size=1)
                                                    
    
    def call(self, inputs):
        x = self.dial_conv(inputs)
        sigm = tf.keras.activations.sigmoid(x)
        tanh = tf.keras.activations.tanh(x)
        m = tf.keras.layers.multiply([sigm, tanh])
        skip_out = self.skip_conv(m)
        res_out = tf.keras.layers.add([inputs, skip_out])
        return res_out, skip_out

class Wavenet(keras.models.Model):
    def __init__(self, init_filters, hid_filters, out_filters, dialation_rates):
        super(Wavenet, self).__init__()
        self.init_filters = init_filters
        self.hid_filters = hid_filters
        self.out_filters = out_filters
        self.dialation_rates = dialation_rates
        self.initial_convolution = keras.layers.Conv1D(filters=self.init_filters,
                                                        kernel_size=1,
                                                        padding="causal")

        self.residual_blocks = []
        for i in self.dialation_rates:
            self.residual_blocks.append(ResidualUnit(self.hid_filters, i))

        self.conv1d_1 = keras.layers.Conv1D(filters=self.out_filters,
                                            kernel_size=1)
        self.conv1d_2 = keras.layers.Conv1D(filters=self.out_filters,
                                            kernel_size=1)

    def call(self, inputs):
        skip_out_list = []
        res_out = self.initial_convolution(inputs)
        for res_layer in self.residual_blocks:
            res_out, skip_out = res_layer(res_out)
            skip_out_list.append(skip_out)
        merged = tf.keras.layers.add(skip_out_list)
        out = tf.keras.activations.relu(merged)
        out = self.conv1d_1(out)
        out = tf.keras.activations.relu(out)
        out = self.conv1d_2(out)
        out = keras.layers.GlobalAveragePooling1D()(out)
        logging.debug(str(out.shape))
        final_out = tf.keras.activations.softmax(out)
        return final_out

    def model(self, input_shape):
        x = keras.Input(shape=input_shape)
        return keras.Model(inputs=[x], outputs=self.call(x))
