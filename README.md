# Wavenet for Classification Using TensorFlow

The model is tested using the WISDM (Wireless Sernsor Data Mining) dataset.  
This dataset includes activity data from 51 participants from an activity
recognition project. Each participant performed each of the 18 activities 
(listed in activity_key.txt) for 3 minutes and the sensor data (accelerometer 
and gyroscope for smartphone and smartwatch) was recorded at a rate of 20 Hz. 
The smartphones used were Nexus 5, Nexus 5X, and Galaxy S6 while the smartwatch
used was the LG G watch [1].

The raw sensor data is located in the raw directory. Each user has its own data
file which is tagged with their subject id, the sensor, and the device.  Within
the data file, each line is:

Subject-id, Activity Label, Timestamp, x, y, z

The features are defined as follows:

subject-id: Identfies the subject and is an integer value between 1600 and 1650.

activity-label: see activity_key.txt for a mapping from 18 characters to the
       activity name

timestamp: time that the reading was taken (Unix Time)

x: x sensor value (real valued) 
y: y sensor value (real valued) 
z: z sensor value (real valued) 

A 3 minute sample of the data from the watch accelerometer (x-axis) is presented below. One can notice the expected difference of walking and eating a sandiwch !   

![png](img/sample_data1.png)

![png](img/sample_data2.png)

Create the conda environment with all the required dependencies and activate it
run the following commands in a bash terminal.

```bash
conda env create -f environment.yml
conda activate tf2
```

In order to train and evaluate the model run the following command

```bash
python main.py
```

For modifications to the hyperparameters of the model refer to the main.py file.

The model performs relatively well with a 90% accuracy. 

```python
model.evaluate(X_test_sc,y_test_oh)
INFO:root:Evaluate model on test set
80/80 [==============================] - 6s 71ms/step - loss: 0.5829 - accuracy: 0.9045
```

The model architecture is illustrated in the figure below

![png](img/model_diag.png)

# References

[1] G. M. Weiss, K. Yoneda and T. Hayajneh, "Smartphone and Smartwatch-Based  
Biometrics Using Activities of Daily Living," in IEEE Access, vol. 7, pp. 133190-133202, 2019, doi: 10.1109/ACCESS.2019.2940729.



