from functools import cached_property
from time import sleep
import pandas as pd
from pandas import DataFrame
import numpy as np
from numpy  import ndarray as Ndarray 
from functools import lru_cache

class Preprocessing:
    def __init__(self, 
                device: str, 
                window_size: int, 
                stride_size: int) -> None:
        """
        Preprocessing class for the WISDM dataset

        Parameters
        ----------
        device : str
            Choose the device you want to get data from.
            Two options available 1) "phone" 2) "watch".
        window_size : int
            Size for the sliding window i.e. sample size. 
        stride_size : int
            Stride for the sliding window.
        """
        self.device = device
        self.window_size = window_size
        self.stride_size = stride_size
    
    @lru_cache(maxsize=1)
    def get_dataframe(self, device: str) -> DataFrame:
        """
        Function to get and preprocess the dataframe
        of the requested device. This function is
        cached so we don't have to rerun it more than once
        (because it is time consuming)
        unless we change the device input variable.

        Parameters
        ----------
        device : str
            Device for which we want data from.

        Returns
        -------
        DataFrame
            The preprocessed dataframe for the requested device.
        """
        gyro_str = 'wisdm-dataset/raw/{}/gyro/data_{}_gyro_{}.txt'
        accel_str = 'wisdm-dataset/raw/{}/accel/data_{}_accel_{}.txt'
        dataframe_lst = []
        for i in range(1600,1651):
            print('subject',i)
            df_g = pd.read_csv(gyro_str.format(device,i,device),
                            names=['person','activity','time','rx','ry','rz'])

            df_g['rz'] = df_g['rz'].str.replace(';','')
            df_g['rz'] = df_g.rz.astype(float)
            df_g.drop(columns=['person'],inplace=True)
            df_g.time = pd.to_datetime(df_g.time,errors='coerce')
            df_g.set_index('time',inplace=True)

            df_a = pd.read_csv(accel_str.format(device,i,device),
                        names=['person','activity','time','x','y','z'])

            df_a['z'] = df_a['z'].str.replace(';','')
            df_a['z'] = df_a.z.astype(float)
            df_a.drop(columns=['person'],inplace=True)
            df_a.time = pd.to_datetime(df_a.time,errors='coerce')
            df_a.set_index('time',inplace=True)

            for j in df_a.activity.unique():
                print('activity',j)
                df_tmp = pd.concat([
                    df_g[df_g.activity == j].resample('0.050S',
                                                        origin='epoch').mean(), 
                    df_a[df_a.activity == j].resample('0.050S',
                                                        origin='epoch').mean()
                                                        ],axis=1).dropna()
                df_tmp['activity'] = j
                df_tmp['person'] = i
                dataframe_lst.append(df_tmp)

        df_final = pd.concat(dataframe_lst)
        return df_final.dropna()

    @property
    def dataframe(self) -> DataFrame:
        return self.get_dataframe(self.device)

    @staticmethod  
    def sliding_window(np_array: Ndarray,
                        window_size: int, stride: int) -> Ndarray:
        """
        Sliding window function.

        Parameters
        ----------
        np_array : Ndarray
            Numpy array that we want to apply the slidding window
        window_size : int
            Size for the sliding window i.e. sample size. 
        stride : int
            Stride for the sliding window.

        Returns
        -------
        Ndarray
        """
        m = (np_array.shape[0] - window_size)//stride + 1
        ind = np.expand_dims(np.arange(window_size), 0) \
            + np.expand_dims(np.arange(m)*stride, 0).T
        return np_array[ind]

    def _windowing(self, inp_df: DataFrame) -> Ndarray:
        tmp_lst = []
        for i in inp_df.person.unique():
            for j in inp_df[inp_df.person == i].activity.unique():
                tmp_lst.append(
                    self.sliding_window(inp_df[(inp_df.person == i) &
                                                (inp_df.activity == j)].values,
                                                self.window_size, 
                                                self.stride_size))
        return np.vstack(tmp_lst)
    
    def get_dataset(self) -> Ndarray:
        return self._windowing(self.dataframe)
