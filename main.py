import os 
import logging
from tkinter.tix import WINDOW
import numpy as np
import pandas as pd
from preprocessing import Preprocessing
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder, MinMaxScaler
import tensorflow as tf
from tensorflow import keras
from wavenet import Wavenet

# when using a GPU
os.environ['CUDA_VISIBLE_DEVICES'] = '0'

logging.basicConfig(level=logging.DEBUG)

DEVICE = 'watch'
WINDOW_SIZE = 256
STRIDE_SIZE = 256

# Get Data
my_prepro = Preprocessing(DEVICE, WINDOW_SIZE, STRIDE_SIZE)
dataset = my_prepro.get_dataset()

# Split the dataset into x and y 
X_dataset = dataset[:,:,:-2]
Y_dataset = dataset[:,-1,-2].reshape(-1,1)
X_dataset = X_dataset.astype(np.float32)

# Split into train and test datasetes with a split ratio
# of 0.2 for the test set
X_train, X_test, y_train, y_test = train_test_split(X_dataset, 
                                                    Y_dataset,
                                                    test_size=0.2,
                                                    random_state=42,
                                                    stratify=Y_dataset)

# Split further the training dataset into training and validation
# with a ratio of 0.1 for the validation set
X_train, X_val, y_train, y_val = train_test_split(X_train, 
                                                y_train,
                                                test_size=0.1,
                                                random_state=42, 
                                                stratify=y_train)

# Transform the categorical data to dense one-hot data 
my_one_hot = OneHotEncoder(sparse=False)

y_train_oh = my_one_hot.fit_transform(y_train)
y_val_oh = my_one_hot.transform(y_val)
y_test_oh = my_one_hot.transform(y_test)


# Normalise the numerical data with a min-max scaler
my_scaler = MinMaxScaler(feature_range=(-1, 1))

train_len, train_window, train_features = X_train.shape
val_len, val_window, val_features = X_val.shape
test_len, test_window, test_features = X_test.shape

X_train_sc = my_scaler.fit_transform(X_train.reshape(-1,6))
X_val_sc = my_scaler.transform(X_val.reshape(-1,6))
X_test_sc = my_scaler.transform(X_test.reshape(-1,6))

X_train_sc = X_train_sc.reshape((train_len, train_window, train_features))
X_val_sc = X_val_sc.reshape((val_len, val_window, val_features))
X_test_sc = X_test_sc.reshape((test_len, test_window, test_features))

# Clear potential residual session data for tensorflow
keras.backend.clear_session()
np.random.seed(42)
tf.random.set_seed(42)


# checkpoint_cb = keras.callbacks.ModelCheckpoint("conv1d.h5",
#                                                   save_best_only=True)
# early_stopping_cb = keras.callbacks.EarlyStopping(patience=50,
#                                                   restore_best_weights=True)

# Initialise Callbacks for early stopping and reducing the learning rate
# when there is a plateau in the validation loss value
reduceLROnPlat = keras.callbacks.ReduceLROnPlateau(monitor='val_loss',
                                                    factor=0.8,
                                                    patience=5,
                                                    verbose=1,
                                                    mode='min',
                                                    min_delta=0.0001,
                                                    cooldown=0,
                                                    min_lr=0.00005)

early = keras.callbacks.EarlyStopping(monitor="val_loss", 
                      mode="min", 
                      patience=50)

# wavenet architcture configuration
INIT_FILTERS = 128
HID_FILTERS = 64
OUT_FILTERS = 18
DILATIONS = tuple(2**i for i in range(8))
RESIDUAL_BLOCKS = 2

# receptive field (filter_width - 1) * sum(DILATIONS) + 1, 
# in this case the filter width is 2

print('receptive field: ', sum(DILATIONS) + 1)

# INitialise WaveNet
mywave = Wavenet(INIT_FILTERS, 
                HID_FILTERS, 
                OUT_FILTERS, 
                DILATIONS*RESIDUAL_BLOCKS)

model = mywave.model(X_train_sc.shape[1:])

model.compile(loss=tf.keras.losses.CategoricalCrossentropy(), 
                optimizer="adam", 
                metrics=['accuracy'])

keras.utils.plot_model(model,
                    show_shapes=True,
                    expand_nested=True,
                    to_file='model_diag.png',
                    dpi=120)

my_history = model.fit(X_train_sc, y_train_oh, epochs=100,
                    batch_size=32, shuffle=True,
                    validation_data=(X_val_sc,y_val_oh),
                    callbacks=[early, reduceLROnPlat])


logging.info('Evaluate model on test set')
model.evaluate(X_test_sc,y_test_oh)
